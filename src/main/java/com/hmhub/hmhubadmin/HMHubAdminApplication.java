package com.hmhub.hmhubadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HMHubAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(HMHubAdminApplication.class, args);
	}

}
